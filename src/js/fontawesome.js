// import the library
import {library, dom} from '@fortawesome/fontawesome-svg-core';

// import your icons
import {faGitlab, faGithub, faMastodon, faMarkdown} from '@fortawesome/free-brands-svg-icons'
import {faEnvelope} from '@fortawesome/free-regular-svg-icons';
import {faCode, faHighlighter} from '@fortawesome/free-solid-svg-icons';

library.add(
    faEnvelope,
    faCode,
    faHighlighter,
    faGitlab,
    faGithub,
    faMastodon,
    faMarkdown
);

dom.watch();
