const path = require('path')
import eslint from 'vite-plugin-eslint'

export default {
    root: path.resolve(__dirname, 'src'),
    plugins: [eslint()],
    build: {
        outDir: '../dist',
        target: [
            'es2015',
            'chrome58',
            'firefox57',
            'safari11',
        ],
    },
    resolve: {
        alias: {
            '~bootstrap': path.resolve(__dirname, 'node_modules/bootstrap'),
        }
    },
    server: {
        port: 8080,
        hot: true
    },
    preview: {
        port: 8081
    }
}
